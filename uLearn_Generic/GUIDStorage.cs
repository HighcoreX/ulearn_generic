﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace uLearn_Generic
{
    class GUIDStorage
    {
        Dictionary<Guid, object> dictionary;
        public GUIDStorage()
        {
            dictionary = new Dictionary<Guid, object>();
        }
        public T CreateObject<T>() where T : new()
        {
            var newobject = new T();
            dictionary.Add(Guid.NewGuid(), newobject);
            return newobject;
        } 
        public List<KeyValuePair<Guid,object>> GetPairs<T>()
        {
            var pairs = dictionary.Where(x => x.Value is T).Select(x=>new KeyValuePair<Guid,object>(x.Key,x.Value)).ToList();
            return pairs;
        }
        public object GetObject(Guid guid){
            object returnedobject = null;
            returnedobject = dictionary.Where(x => x.Key == guid).FirstOrDefault().Value;
            return returnedobject;
        }
    }
}
