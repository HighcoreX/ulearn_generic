﻿
namespace uLearn_Generic
{
    public interface IProcess<TEngine, TEntity, TLogger> {}
    public class Processor<TEngine, TEntity, TLogger> {}
    public class Processor{
        public static ProcessorEngine<TEngine> CreateEngine<TEngine>()
        {
            return new ProcessorEngine<TEngine>();
        }
    }
}