﻿
namespace uLearn_Generic
{
    public class ProcessorEngine<TEngine>
    {
        public ProcessorEntity<TEngine, TEntity> For<TEntity>()
        {
            return new ProcessorEntity<TEngine, TEntity>();
        }
    }
}
