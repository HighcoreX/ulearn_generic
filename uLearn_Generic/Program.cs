﻿using System;
using System.Linq;

namespace uLearn_Generic
{
    class Program
    {
        static void Main(string[] args)
        {
            var MyProcessor = Processor.CreateEngine<MyEngine>().For<MyEntity>().With<MyLogger>();
            GUIDStorage GUIDStorage = new GUIDStorage();
            MyEngine Engine1 = GUIDStorage.CreateObject<MyEngine>();
            MyEntity Entity1 = GUIDStorage.CreateObject<MyEntity>();
            MyLogger Logger1 = GUIDStorage.CreateObject<MyLogger>();
            MyLogger Logger2 = GUIDStorage.CreateObject<MyLogger>();
            var PairsWithTypeMyLogger = GUIDStorage.GetPairs<MyLogger>();
            var Obj1 = GUIDStorage.GetObject(PairsWithTypeMyLogger.First().Key);
            var Obj2 = GUIDStorage.GetObject(Guid.NewGuid());
        }
    }
}